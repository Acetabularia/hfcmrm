\section{Experimental Methods}
\label{sec:em}

The BOLD fMRI signal is determined by both neuro-vascular and neuro-metabolic adaptation, whereas pure hemodynamic readouts such as activity-induced CBF and CBV changes should reflect volume integrated FH responses.
Combining the different fMRI readouts with alternative (invasive) measures of stimulus-evoked activity within the neurovascular unit would provide important insight regarding the interpretation of fMRI signals.
Readouts tightly linked to neuronal activity are electrophysiological recordings including optical imaging using voltage-sensitive dyes, measurements of calcium (Ca\textsuperscript{2+}) transients as prompted by membrane depolarization, and assessment of neurotransmitter turnover (glutamate/glutamine).
Oxygen and glucose consumption assessment, by contrast, is not neuron-specific, but remains nevertheless of interest with regard to neurometabolic coupling.
Electrophysiological recordings have frequently been combined with fMRI.
For example, Logothetis et al. \cite{Logothetis2001} demonstrated a high degree of correlation between the BOLD signal and local field potentials (LFPs) as a measure of overall synaptic input and local processing. While such recordings yield direct insight into the electrical activity of neurons and provide high temporal resolution, they are technologically challenging and do not provide cellular specificity, i.e. other constituents of the neurovascular unit cannot be distinguished.
An attractive alternative is the measurement of Ca\textsuperscript{2+}-transients using optical recordings/imaging in combination with calcium-sensitive dyes \cite{Scanziani2009} or GECIs \cite{Tian2012,Adelsberger2014,Yang2017}, which allow the assessment of cell-specific contributions to neural processing.

The optimal optical method to be combined with the fMRI setup depends on the scientific question to be tackled.
Hence, before designing the hybrid setup the following questions should be addressed:

\textit{{\color{ForestGreen}Is the scientific question asking for truly simultaneous data acquisition?}}
Sequential acquisition with two (or more) modalities may be advantageous if the objects of study are, e.g., evoked brain responses that are highly stereotypic and constant over time.
Given a stimulus paradigm that excludes refractory or adaptation effects, one might consider trial-by-trial variability of neural responses to identical stimuli as random noise, meaning that it does not contain relevant biological information.
Contributions of random noise can be accounted for by averaging over multiple trials.
Under these circumstances, sequential acquisition would be advantageous in order to avoid trade-offs, which, as discussed below, inherently arise in either modality when using a hybrid measurement setup.

However, recent experimental \cite{Stringer2019} and computational studies \cite{Engel2019} suggest that trial-by-trial variability in stimulus-evoked recordings comprises relevant information about the behavioral state and ongoing network dynamics.
As such, assuming that activity is largely spontaneous in nature, meaningful correlation of neural and hemodynamic fluctuations would require simultaneous acquisition and thus multimodal readouts of brain activity.
This is of course also the case for task-free experiments, in which resting-state networks are determined by analyzing fully endogenous and thus “spontaneous” slow-wave fluctuations.

Further, the majority of the studies mentioned in this review are conducted in rodents, which typically require anesthesia.
This affects multimodal experiments twofold: 1. the anesthetic changes spontaneous Ca\textsuperscript{2+} activitiy and 2. The anesthetic changes the breathing pattern and thereby the $CO_2$ levels in the bloodstream, which drastically impacts the BOLD responses \cite{VanAlst2019}.

\textit{{\color{ForestGreen}For simultaneous acquisition, is cellular resolution (or any resolution) required of the optical modality?}}
There are two strategies to gather cell-specific information using optical methods: microscopy-based methods, which provide sufficient spatial resolution to identify individual cells; and bulk sampling methods, which permit identification of specific cell population using selective labelling techniques.
The fMRI signal of a voxel represents the integrated activity of hundreds of thousands of neurons.
Hence, when investigating the contribution of a specific cell population to the lumped fMRI signal, it may be more appropriate to acquire the bulk signal of a volume comparable to that giving rise to the fMRI signal rather than monitoring the behavior of individual cells within a much smaller field of view.
Nevertheless, as already discussed, high spatio-temporal resolution is of relevance for analyzing mechanisms underlying NVC and its dynamics \cite{Devor2011,Uhlirova2016,Gagnon2016}.
For example, MRI/fMRI studies using high spatial resolution (\SI{100x100}{\micro\meter\squared} and \SI{50x50}{\micro\meter\squared}, respectively) in anesthetized rats enabled correlating fluctuations of vessel-specific fMRI signals with the intracellular calcium signal measured in neighboring neurons \cite{He2018}.

Signals reflecting brain activity are typically weak, and the signal-to-noise ratio (SNR) is commonly a limiting factor for true hybrid measurements, i.e. simultaneous recording of signals with two (or more) modalities.
Adapting modalities to work in parallel has its toll on their performance. Boosting up spatial resolution would further compromise SNR, as the signal is proportional to the volume of the individual voxel sampled.
This can be accounted for by using high magnetic field strengths and a small MRI receiver coil yielding a high filling factor (B\textsubscript{0}=14.1 T and 6mm diameter radiofrequency surface coil in the study of \cite{He2018}), which inherently limits the FOV.
For most practical cases, SNR constraints favor using the largest voxel (sample) volume possible, and in case spatial resolution is not required, i.e. the bulk optical signal is sufficient to address the scientific question, fiber photometry provides the highest SNR advantage.
In fMRI, however, the voxel size cannot be increased arbitrarily, as magnetic field variations produced by different tissue types within a larger voxel (partial volume effects) can degrade the SNR.


\textit{{\color{ForestGreen}Are deep lying brain areas relevant to answer the scientific question?}}
Deep brain structures are of relevance when studying signal processing in the brain.
For example, peripheral sensory input is routed via the spinal cord and subcortical nuclei such as the thalamus to the respective somatosensory cortical area.
fMRI, enabling 3D coverage of the whole brain, is ideally suited for such studies.
Yet, can we assume that the relationship between cellular activity measures and the hemodynamic fMRI response, which due to accessibility is typically studied in brain cortex, is independent of the brain region studied?
This is unlikely, as both the cellular composition of brain tissue as well as the local vascular architecture are region-specific and, hence, the weight of their contributions to the fMRI signal will vary across the brain.
In fact, in biophysical modeling of the fMRI signal response, brain region-specific hemodynamic response functions (HRF) have to be considered \cite{Handwerker2004}.
Therefore, we cannot assume that what we have learned for cerebral cortex can be simply extrapolated to other brain regions.

Optical access to deeper brain structures is limited by the nature of light photon interaction with biological tissue (e.g. due to diffusive light propagation and low tissue penetration depth).
There are two approaches to study deep-lying structures with optical techniques:
The use of diffuse optical tomography (including fluorescence tomography), which reconstructs light source distribution in 3D from projections measured at the tissue surface, but which yields spatial information significantly inferior to MRI \cite{Ren_2015}.
Alternatively, invasive procedures have to be used, i.e. superficial brain layers covering the region of interest have to be cleared (e.g. via a cortical window \cite{Pilz2018,Velasco2014} or via the surgical introduction of optical fibers and fiber bundles \cite{Miyamoto2016}).
Individual single- or multimode fibers are probably the least invasive of these approaches and may constitute the method of choice for studying subcortical structures including basal ganglia and brainstem nuclei.


\subsection{{\color{ForestGreen}Methodological aspects}}
\label{sec:ma}

A common though easily overlooked constraint in small animal applications is the potential for set-up incompatibilities between optical tools and the cutting edge of fMRI technologies.
This manifests itself in implant/coil incompatibility, whereby surface coils --- cryogenic coils in particular --- constrain the positioning of animals with optical implants as are canonically used for light-based measurement or stimulation.
Novel manufacturing as well as operation protocol developments permit pitch and yaw variable targeting of structures \cite{stereotaxyz}, though large-scale hybrid measurement or opto-fMRI studies leveraging cryogenic surface coil capabilities have not yet been published.
An additional unexpected pitfall is the magnetic susceptibility of dental cement, which is commonly used to stabilize optic cannulae or skull windows, particularly with regard to longitudinal applications.
For clinical practice, dental cements are commonly adulterated with metal oxides and silicates in order to produce radioopaque characteristics \cite{MontesFariza2016}, which can also lead to artefacts in MRI \cite{Tymofiyeva2013}.
This issue can generally be avoided by using dental cements not explicitly labeled as radioopaque, but as radioopacity is a desired diagnostic trait, pre-implantation cement testing is recommended.
Further, during any implantation procedure, additional care must be taken to not enclose any air pockets or blood clots, as those might similarly introduce susceptibility artifacts in MRI.


