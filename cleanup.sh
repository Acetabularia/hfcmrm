#!/usr/bin/env bash

rm -rf \
	auto_fig_py*.png \
	*.aux \
	*.bbl \
	*.bcf \
	*.blg \
	*.depytx \
	*.log _minted-slides \
	*.nav \
	*.out \
	*.pgf \
	*.pkl \
	*.pytxcode \
	*.run.xml \
	*.snm \
	*.toc \
	*.vrb 
