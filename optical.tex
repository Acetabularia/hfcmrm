\section{Optical Signals for Neuronal Stimulation}
\label{sec:o}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c} %% tabular useful for creating an array of images
			\includegraphics[height=5cm]{img/ofmri}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:ofmri}
		\textbf{Graphical representation of a single-session opto-fMRI workflow, highlighting its sequential integration of optogenetics and fMRI.}
		Panels show the usage of a transgenic strain expressing Cre recombinase (left), viral vector delivery of an optogenetic construct using the Cre/LoxP system and optical cannula implantation targeting the entire transfected system (middle), and fMRI measurement with concurrent light stimulation (right).
		Green represents cells with Cre expression (green arrows indicate structural projections), dark grey dots represent optogenetic construct expression, cyan represents light stimulation and light-evoked postsynaptic activity at the stimulation site, pink represents MR signal.
		Figure adapted from \cite{drlfom}, with permission.
	}
\end{figure}
hen2019}\cite{Chen2019}
\begin{sidewaystable}
	\renewcommand{\arraystretch}{1.1}
	\let\footnoterule\relax
	\scriptsize
	\centering
	\caption{
		\scriptsize
		\label{tab:stim}
		\textbf{Literature overview of opto-fMRI experiments}
		All optogenetic constructs are excitatory and have rapid kinetics, unless otherwise noted.
		Cre-LoxP selection for expression is prepended in parentheses to the promoter, when applicable.
		If the experiment uses vector injection as opposed to constitutive optogenetic construct expression and the injection site differs from the stimulation site, the injection site is prepended in parentheses.
		Paradigms detail in parentheses the characteristics of the smallest structured stimulation train, and original sources should be consulted, as ON periods may contain additional pauses between stimulation trains.
		Step function opsins do not possess canonical within-stimulation-period parameters.
		Abbreviations:
			C1V1 (Chlamydomonas reinhardtii and Volvox carteri chimeric channelrhodopsin, with E122T and E162T mutations),
			CA1 (hippocampus cornu ammonis 1),
			CAG (cytalomegavirus early enhancer/chicken $\beta$-actin),
			CaMKII (calcium-calmodulin kinase II$\alpha$),
			CC (corpus callosum),
			CHRM4 (cholinergic receptor suscarinic 4),
			dfMRI (diffusion fMRI),
			DH (dorsal hippocampus)
			DR (dorsal raphe),
			EF1$\alpha$ (human elongation factor 1 alpha),
			eNpHR3.0 (Chlamydomonas reinhardtii halorhodopsin variant),
			ePET (Pet-1 enhancer),
			ER (event-related),
			FEF (frontal eyefield),
			$f_{rep}$ (laser pulse repetition frequency),
			hChR2 (humanized Channelrhodopsin 2, variant in parentheses),
			hSyn (human synapsin 1 promoter),
			IH (intermediate hippocampus),
			IL mPFC (infralimbic medial prefrontal cortex),
			LH (lateral hypothalamus),
			Mlc1 (myosin alkali light chains),
			MVN (medial vestibular nucleus),
			NAcc (nucleus accumbens),
			PFC (prefrontal cortex),
			PV (parvalbumin promoter),
			rhesus m. (rhesus macaque),
			SSFO (Stabilized Step Function Opsin),
			s.v. (single-vessel),
			TH (tyrosine hydroxylase),
			Thy1 (thymus cell antigen 1),
			Th. (thalamus),
			$\tau_{p}$. (laser pulse width),
			ur. (urethane).
		}
	\vspace{.8em}
	\begin{tabular}{lllllllr}\toprule
		Author, Year		& Optogenetic Construct							& fMRI method		& Promoters 		& Brain Region	& Paradigm ($\tau_{p}$,$f_{rep}$) & Anesthesia & Species \\ \midrule
		Lee et al. 2010\cite{Lee2010}		& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& Th. 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& M1		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& (M1)Th. 	& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& (PV-Cre)CaMKII$\alpha$& M1 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
		Desai et al. 2011\cite{Desai2011}	& ChR2 									& BOLD 			& CaMKII$\alpha$	& S1 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & iso., awake & rat \\
					& ChR2 									& BOLD 			& Thy1			& S1 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & iso., awake & rat \\
		Gerits et al. 2012\cite{Gerits2012}	& ChR2 									& CBV 			& CaMKII$\alpha$ 	& F5 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & awake & rhesus m. \\
					& ChR2 									& CBV 			& CaMKII$\alpha$ 	& FEF 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & awake & rhesus m. \\
		Kahn et al. 2013\cite{Kahn2013}	& ChR2 									& BOLD 			& Thy-1 		& S1		& block (\SI{2.7}{\milli\second}, \SIrange{8}{80}{\hertz}) & iso. & mouse \\
		Weitz et al. 2015\cite{Weitz2015}	& ChR2(H134R)								& BOLD			& CaMKIIa		& DH		& block (\SIrange{5}{50}{\milli\second}, \SIrange{6}{60}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R)								& BOLD			& CaMKIIa		& IH		& block (\SIrange{5}{50}{\milli\second}, \SIrange{6}{60}{\hertz}) & iso.-$N_2O$ & rat \\
		Iordanova et al. 2015\cite{Iordanova2015}	& ChR2(H134R)								& BOLD			& CaMKII$\alpha$ 	& S1 		& block (\SIrange{5}{50}{\milli\second}, \SIrange{3}{20}{\hertz}) & iso. & rat \\
		Liang et al. 2015\cite{Liang2015}	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$	& IL mPFC 	& block, ER (\SIrange{10}{20}{\milli\second}, \SI{10}{\hertz}) & iso., awake & rat \\
		Duffy et al. 2015\cite{Duffy2015}	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& IH 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & med. & rat \\
		Ferenczi et al. 2016\cite{Ferenczi2016} 	& ChR2(H134R)								& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& eNpHR3.0\footnote{inhibitory \label{fn:inh}}				& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& C1V1$_{TT}$								& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& SSFO\textsuperscript{\ref{fn:soe}} 					& BOLD 			& (Th-Cre)EF1$\alpha$  	& PFC 		& block & awake & rat \\
		Lohani et al. 2017\cite{Lohani2017}	& ChR2 									& BOLD, CBV 		& (Th-Cre)EF1$\alpha$ 	& VTA 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso. & rat \\
		Aksenov et al. 2016\cite{Aksenov2016}	& hChR2(H134R) 								& BOLD			& CaMKII$\alpha$	& barrel S1 	& block (unreported, \SI{50}{\hertz}) & awake & rabbit \\
		Albaugh et al. 2016\cite{Albaugh2016}	& hChR2(H134R) 								& CBV 			& CaMKII$\alpha$	& NAcc 		& block (\SI{10}{\milli\second}, \SI{40}{\hertz}) & iso.-med. & rat \\
		Christie et al. 2017\cite{Christie2017}	& hChR2(H134R) 								& BOLD 			& CamKII$\alpha$	& hp. S1 	& ER (\SI{10}{\milli\second}, N/A) & $\alpha$-chloralose & rat \\
		Takata et al. 2018\cite{Takata2018}	& ChR2(C128S)\footnote{step-function opsin, excitatory\label{fn:soe}}	& BOLD 			& Chrm4 		& V1 neurons 	& block & awake & mouse \\
					& ChR2(C128S)\textsuperscript{\ref{fn:soe}} 				& BOLD 			& Mlc1 			& V1 astrocytes & block & awake & mouse \\
		Choe et al. 2018\cite{Choe2018}	& Archaerhodopsin\textsuperscript{\ref{fn:inh}} 			& BOLD			& (L7-Cre)CAG 		& fp. cerebellum& block (\SIrange{5}{20}{\hertz})  & med. & mouse \\
		Brocka et al. 2018\cite{Brocka2018}	& C1V1(E162T) 								& BOLD 			& CamKII$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{25}{\hertz})&  med. & rat \\
					& hChR2(H134R) 								& BOLD 			& (Th-Cre)EF1$\alpha$ 	& VTA 		& block (\SI{10}{\milli\second}, \SI{25}{\hertz})&  med. & rat \\
		Grandjean et al. 2019\cite{Grandjean2019}	& hChR2(H134R) 								& BOLD, CBV 		& (ePet-Cre)EF1$\alpha$ & DR 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso.-med. & mouse \\
		Leong et al. 2019\cite{Leong2019} 	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& MVN 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & iso. & rat \\
		Y. Chen et al. 2019\cite{Chen2019}	& ChR2 									& CBV 			& CAG			& Th.		& block (\SIrange{5}{20}{\milli\second}, \SIrange{3}{10}{\hertz}) & $\alpha$-chloralose & rat \\
		Albers et al. 2019\cite{Albers2019}	& C1V1(E122T/E112T) 							& BOLD, dfMRI 		& CamKII$\alpha$	& fp. S1 	& block (\SI{10}{\milli\second}, \SI{9}{\hertz})  & med. & rat \\
		X. Chen et al. 2019\cite{Chen2019a}	& hChR2(H134R)								& BOLD, CBV (s.v.)	& CAG 			& CA1 		& block, ER (\SIrange{1}{20}{\milli\second}, \SIrange{1}{50}{\hertz}) & $\alpha$-chloralose & rat \\
					& C1V1$_{TT}$ 								& BOLD, CBV (s.v.)	& CaMKII$\alpha$	& CA1		& block, ER (\SIrange{1}{20}{\milli\second}, \SIrange{1}{50}{\hertz}) & $\alpha$-chloralose & rat \\
		Y. Chen et al. 2020\cite{Chen2020}     & hChR2(H134R)                                                          & BOLD          	& CaMKII                & (barrel S1)CC & block (\SIrange{1}{40}{\hertz}, \SIrange{1}{20}{\milli\second}) & $\alpha$-chloralose & rat \\
		Ioanas et al. 2021\cite{Ioanas2021}	& hChR2(H134R) 								& CBV 			& (ePet-Cre)EF1$\alpha$ & DR 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso.-med. & mouse \\
		Ioanas et al. 2021\cite{Ioanas2021}	& hChR2(H134R) 								& CBV 			& (DAT-Cre)EF1$\alpha$ 	& VTA 		& ER, block (\SI{5}{\milli\second} \SIrange{15}{25}{\hertz}) & iso.-med. & mouse\\
		Cover et al., 2021\cite{Cover2021}	& ChR2(H143R)								& BOLD			& hSyn			& IL mPFC	& ER (\SI{5}{\milli\second}, \SI{25}{\hertz}) & awake & mouse \\

		%https://www.pnas.org/content/116/20/10122 Optogenetic fMRI interrogation of brain-wide central vestibular pathways \\
		%\cite{Leong2019}
		%https://pubmed.ncbi.nlm.nih.gov/31182714/ MRI-guided robotic arm drives optogenetic fMRI with concurrent Ca 2+ recording \\
		%\cite{Chen2019}
		%Prefrontal cortical regulation of brainwide circuit dynamics and reward-related behavior DOI:10.1126/science.aac9698 \\
		%\cite{Ferenczi2015}
		%Neural and hemodynamic responses to optogenetic and sensory stimulation in the rat somatosensory cortex
		%\cite{Iordanova2015}
		%aOptogenetic fMRI and electrophysiological identification of region-specific connectivity between the cerebellar cortex and forebrain DOI: 10.1016/j.neuroimage.2018.02.047 \\
		%\cite{Choe2018}
		%\cite{Aksenov2016}i
		%Mapping the Brain-Wide Network Effects by Optogenetic Activation of the Corpus Callosum https://dx.doi.org/10.1093%2Fcercor%2Fbhaa164 \\
		%\cite{Chen2020}
		\bottomrule
	\end{tabular}
\end{sidewaystable}

Discerning how specific cell types affects brain wide functional responses is essential for elucidating the interconnection of brain networks and their roles in information processing and relay.
As already described, light is ideally suited for this purpose due to its minimal interference with the imaging readout method (see \cref{sec:i}) and the fact that brain tissue is not intrinsically sensitive to light.
Manifold molecular biological techniques, rendering tissue specifically susceptible to light photons after light-sensitive sensor protein introduction via genetic engineering, are therefore fully accessible for fMRI.
The combination of optogenetics and fMRI has thus become a method of choice across neuoscientific applications (see \cref{tab:stim}), general concepts and trends of which we further lay out.

Available optogenetic signal transducers include ion channels such as channelrhodopsins \cite{Nagel2002} and ion pumps such as halorhodopsins \cite{Zhang2007}.
Both stimulation and inhibition can be elicited via such constructs, the former most commonly via cation-permeable channelrhodopsins (e.g. ChR2\cite{Nagel2005}) and the latter via anion-transporting halorhodopsins (e.g. eNpHR3 \cite{Gradinaru2010}) or anion-permeable channelrhodopsins (e.g. GtACRs \cite{Mahn2018}).
A further methodological refinement opportunity is provided by step-function opsins, which encompass both anion \cite{Berndt2008} and cation \cite{Berndt2014} channels.
These constructs remain in an altered conformational state (minute-range off-kinetic time constants) following light stimulation and can be returned to the “inactive” conformational state via light stimulation at a different wavelength  ---  as opposed to classic channelrhodopsins, which effectively convert into the “active“ state only during light input (millisecond-range off-kinetic time constants \cite{Gunaydin2010}).

Opto-fMRI applications consist of the sequential integration of optogenetic targeting and the delivery of light concomitant with fMRI measurement.
Though specific targeting for both vector delivery and stimulation can vary greatly, a representative use case of prevalent practices is presented graphically in \cref{fig:ofmri}.
In this example, the co-localization of cell bodies of a widely projecting neurotransmitter system in a  brainstem nucleus is leveraged to stimulate a wide efferent spectrum with a coherent signal.

Vector delivery methods for opto-fMRI use either the Cre-LoxP system in conjunction with AAV (adeno-associated virus) vectors, or lentiviral vectors \cite{Gerits2012} --- whereby the former provides the advantage of decoupling the cell-type selection from the optogenetic construct, allowing greater interrogation flexibility without the need for custom vector production.
{\color{ForestGreen}While Cre-LoxP-based targeting commonly employs transgenic lines, the Cre construct can itself be delivered via a viral vector --- though available virus preparation libraries are more constrained, and there are additional restrictions on promoter length, particularly if delivered via AAV.}

A critical aspect for the success of opto-fMRI experiments is the elicited signal amplitude, as the intrinsically low SNR of the method \cite{MI} constitues a major challenge.
High expression levels, sufficient photon flux density at the target site, and a stimulation protocol yielding high contrast  are essential for obtaining reliable signals.

\subsection{Methodological aspects}
\label{sec:ma}

The first consideration in applying opto-fMRI is selecting the correct biological features for both stimulation and imaging.
While the breadth of neurosicentific applications cannot be suffciently detailed without a review of all neurobiology, a series of guidelines can be formulated based on the characteristics of the method.
Conceptually, opto-fMRI uses targeted, invasive, and high-temporal-resolution stimulation to drive a set of neurons, combined with a whole-brain, non-invasive, medium-temporal-resolution read-out method.
Thus, an important question is whether targeted stimulation is relevant with respect to whole brain imaging.
As a consequence, the ideal setting for leveraging the full potential of the technology is targeting widely-projecting neurotransmitter systems, in order to investigate their effects on neuronal activity at the whole-brain level.
This is substantiated by extant opto-fMRI literature, in which the stimulation of wide efferent spectrum structures such as monoaminergic systems \cite{Lohani2016,Grandjean2019,opfvta,drlfom} and long-range glutamatergic projections \cite{Liang2015,Gao2017} are prominently paired with whole-brain imaging and analysis.
A selected structure with a wide efferent spectrum may however only evoke local activity, such as has been the case for the nucleus accumbens \cite{Albaugh2016}, which provides a setting for contrasting structural, electrically evoked, and optogenetic excitation kinetics.
Opto-fMRI has, on the other hand, also been used with the express intent of stimulating and measuring localized activity, such as in primarily self-projecting cortical regions or in the hippocampus, which is prone to local seizure effects \cite{Christie2013,Duffy2015,Bregestovski2014}.
Such applications, although not leveraging the full spatial potential of fMRI, provide relevant methodological information, support in technology development \cite{Duffy2015}, and yield relevant information with regard to the nature of the fMRI signal \cite{Kahn2013,Lee2010,Chen2019a} (as discussed above).

While fMRI can be freely combined with any optogenetic technologies, the exigences imposed by MRI scanner access specifically encourage the use of the most established and extensively characterized constructs.
As such, novel constructs of great clinical interest, such as anion-permeable channelrhodopsins and step-function opsins, have respectively seen no or little \cite{Ferenczi2015,Takata2018} opto-fMRI use, and present promising opportunities for future research.

A key constraint to leveraging the full versatility of optogenetics, and thus of opto-fMRI, is the application of the technology in species with low or no availability of transgenic lines.
The highly flexible Cre-LoxP/AAV vector delivery method, which allows the decoupling of signal transducer variants from cell-type selection, is contingent on transgenic lines expressing Cre recombinase under the desired cell-type-specific promoter.
Large libraries for such lines are available for the mouse, and increasingly, but to a lesser extent, for the rat.
Other model animals, and particularly higher primates, do not offer any comparable level of access, and consequently, opto-fMRI application in these settings presupposes the use of more unwieldy custom lentiviral vectors, as well as close consideration of the trade-off between promoter length and expression characteristics \cite{Gerits2012,Gerits2013}.

Technical constraints regarding optofMRI, e.g. referring to material incompatibilities, geometric arrangements, etc. have been discussed previously and will not be repeated here.

\subsection{Stimulation Protocols}
\label{sec:sp}

Optogenetics offers considerable flexibility with regard to the stimulation protocol, apt usage of which can considerably enhance the SNR for opto-fMRI.
A fundamental distinction is made in the field of fMRI between “event-related" and “block" designs, which refer to the distribution of stimulation events over the duration of the experiment.
Nominally, a design is deemed event-related if the stimulation (‘ON‘) period consists of a single event or is otherwise equal to or shorter than the acquisition TR --- with longer ON durations being deemed block designs.
Event-related designs lend themselves to sequence randomization, as they provide low but similar contrast upon variation, more closely resembling physiological activation.
While such protocols can be employed in opto-fMRI (and may consist of single events\cite{Christie2017} or short trains\cite{Liang2015}) block designs are generally preferred due to the ability to deliver superior contrast \cite{opfvta,Grandjean2019,fsl,spm}.
In self-stimulation applications, short events can be concentrated into self-stimulation ON periods, yielding designs which are nominally event-related but may residually benefit from block-design contrast characteristics depending on the statistical analysis \cite{Cover2021}.

The precise time-sequence of a block design can further be optimized with respect to both its contrast characteristics and its feasibility for the given biological system being targeted.
The contrast characteristics of the stimulation protocol refer to the theoretical quality of its statistical estimation in the general linear model (GLM), which is the analysis method most commonly used to resolve spatial response patterns based on a stimulation time course.
Experimental parameters which bear on the fitness of a stimulation time course include experiment length, temporal frequency band reliability (lower frequency bands in particular may be considered unreliable in fMRI due to drift), as well as the impulse response function (IRF), which differs greatly with respect to contrasts (BOLD, CBV, or custom functional contrast agents).
These parameters can be submitted to genetic-algorithm optimization workows\cite{Wager2003,Kao2009}, which can produce highly performing stimulation time courses.

The internal structure of stimulus blocks can also be adapted to increase the contrast generated in fMRI, though this may produce varying degrees of physiological comparability in the elicited activity mode.
The intrinsic SNR constraints of fMRI, particularly when performed without cryogenic coils, however, prompt stimulus train optimization towards evoking the maximal activity level compatible with tissue preservation.
Stimulation protocols for opto-fMRI, based predominantly on in-house empirical optimization, generally span \SIrange{10}{100}{\hertz} in frequency and \SIrange{2}{20}{\milli\second} in pulse width \cite{Liu2015,Liang2015,Lohani2016,Albaugh2016,Grandjean2019}.
Such optimizations are formalized in a number of ancillary methodological investigations, which predominantly identify strong effects for stimulation frequency and weak effects for pulse width variations \cite{Liu2015,Grandjean2019,Chen2019,Chen2019a}.
While this is consistent with theoretical considerations arising from extant construct off-kinetics \cite{Gunaydin2010} --- and thus constitutes the best working hypothesis for stimulus structure optimization --- some studies do not reproduce this trend \cite{Chen2020,Iordanova2015}, and further results indicate non-linear and differential neuronal population recruitment based on pulse frequency \cite{Weitz2015}.
Far from simply being a technical parameter for which basic optimization heuristics could reliably be translated into \textit{a priori} optimized protocols across brain areas, stimulus structure may also be leveraged as a flexible tool for the exploration of differential signal propagation.

A ceiling for the maximization of optogenetically evoked signal can be estimated with regard to heating artefacts, which can arise following light stimulation \cite{Rungta2017}.
Heating artefacts are of particular concern for ion-pump optogenetic constructs, as these require considerably stronger light stimulation \cite{Han2011}.
Systematic study of the phenomenon initially reported its emergence at approximately \SI{20}{\milli\watt\per\square\milli\meter} average light deposition per second (\SI{445}{\nano\meter} and \SI{532}{\nano\meter} light stimulation) \cite{Christie2013}, and subsequently at as little as \SI{9}{\milli\watt\per\square\milli\meter} (\SI{445}{\nano\meter} light stimulation) \cite{Christie2017}.
Later still, initial trends were corroborated by a systematic study estimating the emergence of heating artefacts bewteen \SI{19.5}{\milli\watt\per\square\milli\meter} and \SI{25}{\milli\watt\per\square\milli\meter} (\SI{552}{\nano\meter} light stimulation) \cite{Albers2019}.
These estimates may, however, vary at different wavelengths, across brain areas, and be contingent on within-pulse laser power\footnote{Cf. \cite{Christie2017} and \cite{Albers2019}} and stimulation block duration (in addition to the time-averaged light deposition) — and may further depend on the optic cannula diameter in a non-linear fashion \cite{Ash2017}.
Heating artefacts (alongside unspecific visual-activation based signal) thus remain a significant methodological risk in opto-fMRI.
In addition to preliminary empirical verification in a control group, these confounds can be mitigated in General Linear Model (GLM) analysis given sufficient control group size \cite{fsl,spm,opfvta}.

An important consideration throughout stimulation protocols is that optogenetics can \textit{drive}, but cannot \textit{clamp} activity.
This means that while both excitation and inhibition can be elicited, this does not happen in a complementary manner.
In the absence of activation-inducing stimuli, cells are not inactivated, but rebound towards endogenous network activity.
Similarly, in the absence of inactivation-inducing stimuli, cells will also rebound towards normal endogenous network activity.
This constraint even applies to step-function opsins \cite{Berndt2014}, which work as ion channels, and whose primary advantage is requiring minimal light to switch into an “open channel” state.
Step-function optogenetic techonlogy is theoretically feasible for providing \textit{clamping} functionality with minimal light deposition in a fused-protein assay, though such tools are not currently available, and functionality approaching clamping is only available via construct co-delivery \cite{Berglund2019}.

\subsection{Opto-fMRI in the study of monoaminergic signaling}
\label{sec:ofsms}

In preclinical neuropsychiatric applications, opto-fMRI has prominently been used to map the effective connectivity of neurotransmitter systems, as illustrated by the landmark results of mapping the dopaminergic \cite{Lohani2016} and serotonergic \cite{Grandjean2019} systems (\cref{fig:vta,fig:dr}).
On account of better statistical contrast, such mapping efforts produce more finely resolved spatial maps than corresponding chemogenetics approaches (cf. \cite{Grandjean2019} and \cite{Giorgi2017}), albeit at the cost of increased invasiveness.

The reuse of these assays has demonstrated map reproducibility \cite{Grandjean2019,drlfom}, qualitative translational consistency \cite{Lohani2016,opfvta}, as well as applicability to psychopharmacological profiling \cite{drlfom}.
Further,  opto-fMRI has facilitated the disambiguation of dopaminergic and non-dopaminergic Ventral Tegmental Area (VTA) signalling in reward circuitry \cite{Brocka2018}, and has uncovered significant deviations in functional dopaminergic VTA connectivity from what could linearly be inferred based on structural connectivity \cite{Lohani2016}.
A core advantage which renders such studies illuminating beyond simple SNR incrementation compared to chemogenetic or resting-state fMRI, is that it constitutes a qualitative leap towards the \textit{ceteris paribus}\footnote{i.e. “other things being held constant”, as commonly used to characterise the generalizability of statistical regularities \cite{Oulis2018}.} estimation of effective connectivity.
Being able to drive neuronal subpopulations directly, the causal functional effects --- rather than merely the correlative functional features --- of activity in a system can be clearly highlighted (cf. “effective connectivity” and “functional connectivity” \cite{Friston2011}).
Although the limited temporal resolution of current fMRI technology renders an elucidation of second and higher order downstream signal relay infeasible, opto-fMRI is currently able to map out neuronal-subpopulation-based signal propagation one step at a time.
Such step-wise functional projection profiles can further be conceptualized as phenotypical characteristics (i.e. “functional neurophenotypes” \cite{imstpc}) and be subject to experimental manipulation.

A particular feature in the stimulation and whole-brain assay of long-ranging projections, including but not limited to monoaminergic projections, consists in the ability to differentiate responses across subcellular compartments at macroscopic resolutions --- \textit{in vivo} and non-invasively, with respect to the measurement modality.
Given sufficiently high statistical contrast, combined with cell-type selective stimulation as achieved via opto-fMRI, neuronal compartments can be observed to show different response valences or kinetics.
This is represented by a graphical model in \cref{fig:cb}, and substantiated by experimental results which showcase somatic and post/synaptic voxels displaying either similar (\cref{fig:vta}) or opposite (\cref{fig:dr}) responses.
Hence, targeted activation of dopaminergic neurons in the VTA prompts post-synaptic excitation, while targeted activation of serotonergic neurons in the DR prompts post-synaptic inhibition.

Such localization is particularly relevant in the study of neuropsychiatric interventions, where targets may be differentially distributed across neurons (see receptor distributions in \cref{fig:cb}).
Correspondingly, proof of principle applications of opto-fMRI have been able to deliver cell-compartment level profiling of both acute \cite{Grandjean2019} and chronic antidepressant action, and have shown differential longitudinal profiles for somatic, cortical synaptic, and subcortical synaptic areas \cite{drlfom}.

On account of being neuronal subpopulation-selective as well as contingent on the induction of signal amplitudes sufficiently high to overcome the SNR constraints of fMRI, opto-fMRI commonly elicits neurotransmitter depletion.
This manifests itself in incremental response-amplitude reduction (i.e. biologically meaningful refractory effects), as seen in \cref{fig:dr_ts}.
Though extant literature commonly handles this feature as an ancillary nuisance process, the ubiquity of this phenomenon constitutes a representative example for an implicit assay of a neurophysiologically relevant process, which can be reexamined in refined analysis instantiations.
More generally, given the spatial standardization presupposed by whole-brain imaging, opto-fMRI characteristically produces manifold possibilities for quantitative data reuse and integration, as otherwise uncommon in the study of cell biological assays.

\begin{figure}
	\begin{centering}
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/vta.png}
		\caption{
			Population-level t-statistical map of right Ventral Tegmental Area dopaminergic neuronal stimulation.
			Figure adapted from \cite{opfvta}, with permission.
			}
		\label{fig:vta}
		\vspace{.4em}
	\end{subfigure}
	\hspace{.6em}
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/dr.png}
		\caption{
			Population-level t-statistical map of Dorsal Raphe Nucleus serotonergic neuronal stimulation.
			Figure adapted from \cite{drlfom}, with permission.
			}
		\label{fig:dr}
		\vspace{.4em}
	\end{subfigure}
	\end{centering}
	\begin{subfigure}[b]{\textwidth}
		\vspace{1.2em}
		\centering
		\includegraphics[width=\textwidth]{img/dr_ts}
		\caption{
			Single-subject time course of mean signal from the Dorsal Raphe Nucleus region of interest, during optogenetic stimulation of serotonergic neurons.
			CBV signal trace shown in blue, regressor traces for statistical modelling, such as seen in \cref{fig:dr}, shown in orange and green.
			Figure adapted from reference analysis results of the SAMRI package \cite{samri,irsabi}.
			}
		\label{fig:dr_ts}
		\vspace{-.2em}
	\end{subfigure}
		\vspace{.4em}
		\caption{
			\textbf{Opto-fMRI can be leveraged to image multimodal activity patterns elicited by widely projecting neuronal systems with low endogenous activity profiles}.
			Optogenetics permits both neurotransmitter-specific selectivity, which can be used to target specific neuronal subpopulations, as well as high-amplitude signal enhancement, which can drive network population to sufficiently high levels of activity, as to be clearly modeled in time-resolved fMRI data.
			Depicted are both population-level activity maps (\cref{fig:vta,fig:dr}), showing uniform and divergent valence of responses, respectively, as well as a subject-level signal trace example (\cref{fig:dr_ts}).
			}
		\label{fig:three graphs}
\end{figure}


\begin{figure} [ht]
	\centering
	\includegraphics[width=\textwidth]{img/ma_voxels}
	\vspace{.2em}
	\caption{
		\textbf{Opto-fMRI provides macroscopic resolution disambiguation of cell biological processes}
		Depicted are neuronal schematics showing a somatic compartment and a synaptic compartment which depending on the statistical contrast of the stimulation and the neuronal system targeted may be outside the effective spatial autocorrelation range of fMRI.
		Voxels are color-coded analogously to the color areas seen in \cref{fig:dr}.
		The neuronal schematic showcases cell biological processes, such as neurotransmitter synthesis, anterograde synaptic transmission, autoinhibition, neurotransmitter reuptake, and degradation, laid out over cell compartments.
		Figure adapted from \cite{drlfom}, with permission.
		\label{fig:cb}
		}
\end{figure}


