#!/usr/bin/env bash

TARGET="${1}"
ALL="
	main.tex
	review.tex
	"

if [ $TARGET = "all" ] || [ "$TARGET" == "" ]; then
	for ITER_TARGET in *.tex; do
		if [[ $ALL =~ (^|[[:space:]])$ITER_TARGET($|[[:space:]]) ]];then
			ITER_TARGET=${ITER_TARGET%".tex"}
			./compile.sh ${ITER_TARGET}
		fi
	done
else
	pdflatex -shell-escape ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	bibtex ${TARGET} &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex
fi
