<img src="https://nph.msubmit.net/images/NPH_email_banner.jpg">

Dear Dr. Ioanas,

Your paper entitled "Hybrid fiber optic-fMRI for multimodal recording and manipulation of brain activity in rodents" has been reviewed and may be suitable for publication in Neurophotonics subject to some revisions suggested by the reviewers.

The reviewers' comments are included below. Please make these corrections and reply to the critique on a point-by-point basis. Then go to the following URL to submit the revised version:

https://neurophotonics.msubmit.net/cgi-bin/main.plex

The revised manuscript should be submitted by: December 24, 2021.

Please be sure to adhere to the following formatting requirements when submitting your revised manuscript:

As of 1 January 2020, Neurophotonics is now requiring authors to submit their manuscripts with structured abstracts. We ask that you format your abstract as a structured abstract per the guidelines available here:

https://www.spiedigitallibrary.org/journals/neurophotonics/author-guidelines

Please submit your revised manuscript in either Microsoft Word or LaTeX. These are the only file formats that are usable for the composition process. Mark the revised manuscript to show where you have made changes in response to the reviewer comments. You may use any of the following methods of marking your changes: highlighting, colored fonts or underlined text.. <B>Do not use Track Changes, strike-throughs, or similar editing tools.</B>

<B>Include a Figure Caption page at the end of your manuscript file.</B>

Individual Figure Guidelines:

<li> All figure parts should be included in one file, on one page. For example, if Figure 1 contains three parts (a, b, c), then all of the labeled parts should be combined in a single file for Figure 1.</li>

<li> Captions should be listed separately within the manuscript and contain descriptions of all labeled figure parts (a), (b), etc. Do not include captions or figure numbers in the image file.</li>

<li> All figures should have resolution of 300-600 pixels per inch (ppi). Enlarge to 150% to check for jagged or blurry lines, indicating low resolution.</li>

<li> Avoid graphs with shaded, transparent, or grid backgrounds. The background should be white. If colored lines are used in graphs, please add symbols or dot-dash textures to distinguish the lines.</li>

<li> Text should be no smaller than 8 pt at final typeset size. Use a clear and readable font such as Times, Arial, or Symbol.</li>

<li> Ensure that line weights will be 0.5 points or greater in the final published size. </li>

<li> File size should not exceed 2-3 MB per figure.</li>

<li> Figures should be in PNG, TIFF, PostScript, EPS, or PDF. Do not submit your figures as Word documents.</li>

(Note: Your individual figure files will be merged with your manuscript text for the reviewers.)

<li> Do not upload Tables as separate figure files. Please leave them in the manuscript.</li>

<li>The following video formats are acceptable: MOV (.mov), MPEG (.mpg), and MP4 (.mp4). Video files should not exceed 12 MB.
Please visit https://www.spiedigitallibrary.org/journals/neurophotonics/author-guidelines#divVideo for video file submission instructions.</li>

<li> Please make certain that your manuscript contains a Conflict of Interest statement. If you have used either human subjects or animals in your experiments, a statement is required indicating that your study has been approved by a governing body at your institution. For more information regarding these policies, please visit our website: https://www.spiedigitallibrary.org/journals/neurophotonics/author-guidelines#divConflictofInterest</li>

<li> Biographies: Biographies of authors are included with each published paper. Manuscripts should include a brief professional biography not to exceed 75 words. Authors are also encouraged to update their online SPIE profile on our website at https://spie.org/app/profiles/default.aspx with a brief biography and photograph.</li>

<li> Supplemental Material: If your submission contains supplemental material, please follow the guidelines available here:
https://www.spiedigitallibrary.org/journals/neurophotonics/author-guidelines</li>

Article Publication Charge:

<li>Authors of papers accepted for publication in <I>Neurophotonics</I> are required to pay a mandatory open access fee of $1675. Discounts may apply (see https://www.spiedigitallibrary.org/journals/open-access for more information about our discount program). You will receive an email with instructions on how to pay your article publication charge if your manuscript is accepted for publication.rtant, as your paper will not be published until payment is received. </li>

<B>Please note that only the corresponding author can submit the revised manuscript.</B> If that is not possible, the corresponding author should contact the journal editorial office to request that another author be given permissions to submit the manuscript.

If you have any questions, feel free to contact the journal staff at neurophotonics@spie.org. Thank you for submitting your manuscript to Neurophotonics.

Sincerely,

Xin Yu
Associate Editor
Neurophotonics


------------------------------------------------------------------------------


Reviewer Comments:

Reviewer #1 (Reviewer Comments Required):

This manuscript provides a comprehensive review of methods to combine functional magnetic resonance imaging with optical measurement and stimulation. Although the review is quite comprehensive and the content appropriate, I found the format quite difficult to follow and I would suggest some changes regarding that. More specifically, the current format mixes a listing of multimodal setups with going into detail for highly specific research questions. Below I suggest an alternative format that in my view will enhance readability. I also have some other minor comments.


The current format of the manuscript is the following:
1. Introduction
2. Experimental methods
2.1 Multimodal recordings: Method selection
2.2 Hybrid optical/fMRI setups
3. Multimodal readouts of brain activity - insights into neurovascular coupling
3.1 components of neurovascular coupling
3.2 Multimodal imaging for refining forward modelling
4. Optical signals for neuronal stimulation
4.1 Methodological aspects
4.2 Stimulation protocols
4.3 Opto-fMRI in the study of monoaminergic signalling
5. Conclusion

I suggest this changes to something like this:
1. Introduction
2. Experimental methods
2.1 Multimodal recordings: Method selection (Get rid of the subsection, all of it is simply section 2)
2.2 Hybrid optical/fMRI setups (Move this info to the relevant topics further down)
3. Relevant Research Example: Multimodal readouts of brain activity - insights into neurovascular coupling
3.1 components of neurovascular coupling
3.2 Multimodal imaging for refining forward modelling (Include relevant paragraphs from previous section 2.2)
4. Relevant Research Example: Optical signals for neuronal stimulation
4.1 Methodological aspects (Include relevant paragraphs from previous section 2.2)
4.2 Stimulation protocols (Combine into section 4.1)
4.3 Opto-fMRI in the study of monoaminergic signalling (Include relevant paragraphs from previous section 2.2)
5. Conclusion

Additional comments:
&#x2022; Page 2, second paragraph, first sentence "Animal models play an important role...": Maybe briefly mention which models are most suitable or widely used or something.
&#x2022; Page 2, Last paragraph "However, as electrophysiology lacks cell specificity and interferes with MRI, is has gradually been replaced by rapidly developing optical techniques.": Typo
&#x2022; Page 13, Section 4.1: Technical constraints regarding opto-fMRI are not mentioned (They are in section 2.2), I think they should either condense section 3 and 4 and provide them as research examples in section 2 when detailing the used hybrid setup in question, OR focus on these sections with research questions and then detail the methodology in this section.



Reviewer #2 (Reviewer Comments Required):

The authors present a review on multimodal neuroimaging combining fiber-based optical/optogenetic tools with fMRI. I want to congratulate the authors for this excellent manuscript. It was a pleasure to read. The authors cover the topic comprehensively, while stayin concise and providing suitable examples where helpful.
I have a few minor comments, the authors should address:
1. References in the tables are not numbers or linked to the reference list. Adding numbers might be helpful for the reader.
2. In the second last para of section 4.1 the authors discuss Cre-LoxP. They state that not many rat Cre lines are available. It could be added that also in naive rat (and mice) Cre can be used after application via AAV. with a suitable promoter, cell-specificity can be reached if the opsin/sensor is subsequently administered via AAV - lentiviral vectors would not be required.
3. Labels in Fig. 4 are small and hard to read on a small reader without zooming in.
4. Legend to figure 7 is confusing. Frankly, I did not understand the link to Fig. 6. Also symbols in the figure should be explained in the legend.
5. Finally, the title is somewhat understating the content of the paper. I was actually (positively) surprise about the broad content of the paper. O fcourse, choosing a title is the authors decision. However, adding terms like cell-specific or metabolic might make the article even more attractive to a wide number of readers.
