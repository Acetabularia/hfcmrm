\section{Multimodal readouts of brain activity - insights into neurovascular coupling}
\label{sec:nvc}


\subsection{Components of neurovascular coupling}
\label{sec:nvc1}

The basic neurovascular unit (NVU) consists of neuron, astrocyte and vascular cells (such as endothelial cells, pericytes, and smooth muscle cells), and is schematically summarized in \cref{fig:nvu}.
The concept of CBF regulation in response to an increased energy demand by active neurons has been revisited in view of the fact that neurons can maintain their initial activity without requiring additional blood supply and, as a consequence, CBF responses are slow.
Neurotransmitter-mediated signaling plays a major role in regulating CBF, the main excitatory neurotransmitter being glutamate.
It has been suggested that increased extracellular levels of glutamate following its release at synapses switch astrocytes to anaerobic glycolysis.
The relative amount of O$_2$ available for neurons thereby increases, while astrocytic lactate would serve as an energy substrate for neurons (a process known as the “astrocyte-neuron lactate shuttle”) \cite{Pellerin1994}.
Demonstration of a lactate gradient from astrocytes to neurons supports this hypothesis \cite{Mchler2016}.
CBF adaptation would then serve to replenish the (astrocytic) energy reservoirs, but also to maintain function during prolonged stimulaion \cite{Schulz2012,Mishra_2016}.

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[height=5cm]{img/NVU.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:nvu}
		\textbf{Schematic of cellular interactions mediating neurovascular coupling.}
		Excitatory input triggers synaptic release of glutamate (Glu), which activates neuronal NMDA receptors (NMDA-R) as well as astrocytic ion channels  and metabotropic Glu receptors (e.g., mGluR5), prompting the release of vasodilator substances such as nitric oxide (NO), epoxyeicosatrienoic acids (EETs), prostaglandins (PGs).
		The vasoactive compounds interact with capillary pericytes (and arteriole and pial artery smooth muscle cells).
		Local capillary dilation may also result from direct interaction with endothelial cells (EC) and then be back-propagated to feeding arteries/arterioles via hyperpolarization and mediators such as NO.
	}
\end{figure}


\subsubsection*{Astrocytes as mediators of functional hyperemia}
\label{sec:nvc1a}

The involvement of astrocytes in NVC has long eluded electrophysiological recordings, as they are electrically inexcitable.
However, given the key location within the NVU, engulfing the synapses and covering the vasculature with their end-feet, it appears plausible that astrocytes are involved in FH to an extent beyond the mere role of an energy reservoir.
Glutamate mediated signaling leads to activation of neuronal N-methyl-D-aspartate receptors (NMDA-R) as well as astrocytic metabotropic glutamate receptors (mGluR) and ion channels \cite{Attwell2010}.
Down-stream processes involve the activation of neuronal nitric oxide synthase (nNOS) and astrocytic phospholipase A2 (PLA2), leading to the formation of nitric oxide (NO) and arachidonic acid derivatives (prostaglandins, PGs, and epoxyeicosatrienoic acids, EETs) --- compounds which are known as vasodilators \cite{Attwell2010}.
These early findings provided a strong indication of astrocyte involvement in FH.

Investigating the intricate role of astrocytes in the generation of BOLD signals is a prime example of a research question that strongly benefits from hybrid optical/MRI approaches.
Their involvement could be demonstrated by combining neuronal and astrocytic Ca\textsuperscript{2+} recordings with BOLD fMRI \cite{Schulz2012}.
Previous studies have shown a sluggish astrocytic Ca\textsuperscript{2+} response to somatosensory stimuli, with a much longer time-to-peak and decay time relative to the neuronal response (\cref{fig:astro}a).
Such fiber-optic bulk recordings represent the averaged activity of all astrocytes in the area around the fiber tip.
It should be noted that individual astrocytes can vary in their temporal characteristics, which can be observed under two-photon microscopy \cite{Stobart2016}.
Yet, within the spatial scale of BOLD fMRI voxels, it becomes apparent that the kinetics of the summed astrocytic Ca\textsuperscript{2+} response correspond to the later phases of the BOLD response (\cref{fig:astro}b) --- i.e. the prolonged signal elevation after stimulus cessation, and the slow return to baseline.
These so-called nonlinear components of the BOLD response have long remained enigmatic as they do not linearly scale with the neuronal response, unlike the BOLD amplitude and time to peak.
A growing body of NVC research aims to characterize the diverse roles performed by astrocytes.
Recordings of intrinsic astrocytic Ca\textsuperscript{2+} activity concurrent with BOLD fMRI, \cite{Wang2018} have found spontaneous Ca\textsuperscript{2+} transients to be coupled with cortex-wide negative BOLD signals and a positive BOLD signal in the thalamus.
Subsequent elctrophysiology confirmed that thalamic activity preceded the Ca\textsuperscript{2+} transients, with astrocytes thus appearing to act as a mediator of thalamic regulation of cortical states.
Further, optogenetic stimulation of astrocytes has found to be sufficient to evoke positive BOLD responses, without significant activation of nearby neurons \cite{Takata2018}. While the physiological role of such independent astrocytic activation is still unclear, it may provide an explanation for certain task-related hemodynamic responses that occur without neuronal activity \cite{Sirotin2009}.


\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[height=5cm]{img/astrosignal.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:astro}
		\textbf{Neuronal and astrocytic signals differ in time course, adding up to the cumulative BOLD response.}
		Forepaw stimulation a mouse under ketamine/xylazine anesthesia, stimulated in \SI{8}{\second} blocks (gray shaded area), with an internal frequency of \SI{3}{\hertz}, an amplitude of \SI{0.7}{\milli\ampere}, and a pulse duration of \SI{0.5}{\milli\second}.
		(a) Normalized Ca\textsuperscript{2+} transients of neuron (blue) and astrocyte (green) population.
		Note post-stimulus undershoot in astrocytic Ca\textsuperscript{2+} response.
		(b) BOLD response with black dots indicating experimental data points and thick black solid fitted curve comprising weighted contributions from neurons (blue) and astrocytes (green) contribution as derived from Ca\textsuperscript{2+} recordings convolved with the respective hemodynamic response functions (HRF).
		The HRFs were assumed as cell-type specific gamma-variate functions.
		Adapted from Skachokova et al. 2021 (pending publication), with permission.
	}
\end{figure}


\subsubsection*{Vasodilatory signals backpropagate along the vasculature}
\label{sec:nvc1b}

Electrical forepaw stimulation was shown to prompt a local dilation of the capillary bed and an increase in the velocity of red blood cells indicative of decreased vascular resistance \cite{Stefanovic2007}.
Whether this dilation is passive or active is currently unclear, though the effect appears to be controlled by pericytes \cite{Peppiatt2006,Hamilton2010}.
Recent data support the important role of pericytes in NVC, demonstrating that astrocytes signal to pericytes rather than arterioles \cite{Attwell2010}, and that pericyte degeneration leads to neurovascular uncoupling \cite{Kisler_2017}.

While neuronal and astrocytic vasodilatory signaling is focused to the site of activity, spatiotemporal analysis of vascular responses revealed involvement of vessels at distances larger than 1mm \cite{Hillman2014,Li2003} via retrograde propagation of vasodilation \cite{Chen2014}.
The endothelial cell (EC) layer constitutes the obvious guidance structure for signal backpropagation to feeding arterioles and pial arteries, as disruption of EC signaling led to significantly reduced FH.
Two vasodilatory mechanisms have been suggested: a fast process mediated by endothelial hyperpolarization and a slow process associated with the release of vasodilator compounds \cite{Hillman2014}.

Taken together, FH arises as combination of different mechanisms affecting vascular segments in a differential manner \cite{Attwell2010,Hillman2014,Mishra_2016}.
Neurotransmitter (glutamate) release triggers a sequence of events that lead to an orchestrated response of neurons, astrocytes, and vascular cells (ECs, pericytes, smooth muscle cells) causing local FH.
The vasodilatory stimulus is then backpropagated to arterioles and pial arteries via EC signaling.
As the vasculature is dilating along the entire path of this backpropagation, the resulting spatial blurring can make fMRI responses appear more widespread than the underlying neural activitiy \cite{OHerron2016}. In addition, the vascular organization differs greatly across brain regions, which may be a key contributor to the regional variability of the HRF observed in fMRI studies \cite{Handwerker2004}.
To date, studies involving vascular signal propagation have been exclusively conducted with high-resolution microscopy, focusing on individual blood vessels. With the continual improvements of GECIs and gene targeting, it has become possible to apply the multimodal techniques discussed in this review to other cell types, such as various vascular cells \cite{Wier2017}.
Given the integrative nature of the fMRI signal, multimodal recording of activity-induced cellular responses becomes mandatory for the deconvolution of individual contributions governing the fMRI signal response.



\subsection{Hybrid optical/fMRI applications} 
\label{sec:hybridapp}



\subsubsection*{Fiber photometry and fMRI}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[height=5cm]{img/fpsetup.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:fpsetup}
		\textbf{Fiber photometry is based on an advanced yet robust multi-component system leveraging optical fiber light transmission in order to separate MR and optical instrumentation:}
		A laser beam is coupled into a fiber-optic patch cable that is connected to an implant on the mouse;
		fluorescent emissions are guided back through the same fiber;
		the entire optical setup is located outside of the MR scanner room.
		MRI volumes, the fluorescence time course, and the stimulation protocol are then combined in offline analysis.
		Abbreviations:
			PMT (photomultiplier tube),
			EF (emission filter),
			DM (dichroic mirror),
			CL (coupling lens),
			FP (fiber port),
			ND (neutral density),
			DAQ (data acquisition device).
		Adapted from \cite{Schlegel2018}.
	}
\end{figure}

{\color{violet}
Fiber photometry (\cref{fig:fpsetup}) is ideally suited for simultaneous readout with fMRI and has so far been the most popular technique for in-vivo hybrid recordings in rodents (see \cref{tab:recording}).
An optical fiber can guide the laser beam into the MR bore, and the fluorescence signals to a detector outside of the magnetic field, so that the two methods do not interfere with each other.

While a single multimode fiber lacks spatial resolution, the placement of the fiber tip allows regional selectivity for a given brain region.
Further spatial specificity can be gained by selecting the core diameter and numerical aperture of the multimode fiber to adjust the size of the cone where light is emitted and collected, such that it matches the area of interest.
Additionally, an array of fibers can be implanted so as to target multiple brain regions of interest, including subcortical areas \cite{Sych2019}.
Cell type or cell compartment specificity can be attained by expressing GECIs under the appropriate promoters \cite{Rose_2014,Tian_2012}.

The hardware for fiber photometry is largely interchangeable with the optogenetic stimulation tools discussed in the subsequent sections.
Thus, the two methods can readily be combined, allowing multimodal readouts under optogenetic control of defined neuronal circuits \cite{Wang2018,Albers2017}.


\subsubsection*{Microscopic optical imaging and fMRI}
Combining the excellent spatio-temporal resolution of fluorescence microscopy with the full brain coverage of fMRI would provide a useful bridge between the microscopic, mesoscopic, and macroscopic scale.
However, optical imaging systems contain many sensitive electronic components and actuators that cannot operate in an MR environment.
Designing hybrid systems in which the optical equipment satisfies the material and spatial constraints of MRI, thus remains an ongoing challenge.
In particular, canonical RF coil designs may also interfere with the light path.
Nevertheless, a proof-of-concept for an MR-compatible two-photon microscope has been developed, in which the optical components are connected  to the MR bore via a fiber light guide \cite{Cui2017}.
In a recent study, a fiber-optic bundle was used to project the fluorescence signals on a camera outside the MR room, allowing widefield mesoscopic imaging of the entire mouse cortex \cite{Lake2020}.
Another prototype used a miniaturized MR-compatible camera, allowing a one-photon microscope to operate within the MR bore \cite{Wapler2021}.


\subsubsection*{Diffuse optical imaging and fMRI}
As an alternative to fluorescence-based recordings, the absorption properties of hemoglobin can be used as a functional readout of blood volume and oxygenation changes.
NIRS, owing to the attractive properties of near-infrared light,  has found widespread use as a non-invasive tool in clinical applications \cite{Kim_2017}.
The high penetration depth, which enables it to pass through even the human skull, and the ability to provide absolute quantification of hemodynamic parameters, have made it an early tool of choice to validate assumptions made with BOLD fMRI \cite{Steinbrink_2006}.
Hybrid NIRS/fMRI systems can also expand the scope of MRI applications without the need for exogenous contrast agents \cite{Hashem_2020}.
Typically, a setup consists of one or more light source/photodetector pairs, which, as in fiber-photometry, can be connected via fiber-optic cables.
Typically, the recordings are not spatially resolved, as the long, diffusing light paths inherently limit the resolution, though camera based systems have been developed \cite{Zhu_2016}.
Ultimately, as another hemodynamic readout, NIRS cannot provide the complementary information needed to separate the various contributions to BOLD  fMRI, which thus limits its utility for NVC research.


\begin{sidewaystable}
	\renewcommand{\arraystretch}{1.1}
	\let\footnoterule\relax
	\footnotesize
	\centering
	\caption{
		\label{tab:recording}
		\textbf{Studies using hybrid systems for optical Ca\textsuperscript{2+}-recording and fMRI in-vivo.}
		Abbreviations:
			ASL (arterial spin labeling),
			fp. (forepaw),
			FP (fiber photometry),
			hp. (hindpaw),
			iso. (isoflurane),
			LGN (lateral geniculate nucleus),
			med. (medetomidine),
			Po (posterior thalamic nucleus),
			RS (resting state),
			S1 (primary somateosensory cortex),
			SC (superior colliculus),
			stim. (stimulation),
			s.v. (single-vessel),
			ur. (urethane),
			WI (widefield imaging).
		}
	\vspace{.8em}
	%\begin{tabular}{lcccccccc}\toprule
	\begin{tabular}{lllllllr}\toprule
		Author, Year		& Optical Method& fMRI Method		& Target Cells 				& Brain Region & Paradigm & Anesthesia & Species \\ \midrule
		Schulz et al. 2012\cite{Schulz2012} 	& FP 		& BOLD 			& unspecific Ca\textsuperscript{2+} 	& fp./hp. S1 & fp./hp. stim. & iso. & rat  \\
		Liang et al. 2017\cite{Liang2017} 	& FP		& BOLD 			& neurons 				& SC & visual stim. & med. & rat \\
		Schwalm et al. 2017\cite{Schwalm2017} 	& FP 		& BOLD 			& neurons 				& fp. S1, Po & RS & iso. & rat \\
		He et al. 2018\cite{He2018} 		& FP 		& BOLD, CBV (s.v.)	& neurons 				& fp. S1, vibrissa S1 & fp., RS & $\alpha$-chloralose & rat \\
		Schlegel et al. 2018\cite{Schlegel2018} 	& FP 		& BOLD 			& neurons, astrocytes 			& hp. S1 & hp. stim., RS & iso. & mouse \\
		Wang et al. 2018\cite{Wang2018} 	& FP 		& BOLD			& astrocytes				& fp. S1 & RS & ur. & rat \\
					& FP 		& BOLD			& astrocytes 				& fp. S1 & fp. stim. & med. & rat \\
					& FP 		& BOLD			& neurons, astrocytes 			& fp. S1 & fp. stim., RS& $\alpha$-chloralose & rat \\
		Chen et al. 2019\cite{Chen2019} 	& FP 		& BOLD 			& neurons 				& barrel S1 & optogenetic stim. & $\alpha$-chloralose & rat \\
		Tong et al. 2019\cite{Tong2019} 	& FP\footnote{within-subject 2-site optical measurement} 	& BOLD 			& neurons 				& SC, LGN & visual stim., RS & med. & rat \\
		Van Alst et al. 2019\cite{vanAlst2019} 	& FP 		& BOLD, ASL 		& neurons 				& fp. S1 & fp. stim. & iso.-med. & rat \\
		Lake et al. 2020\cite{Lake2020} 	& WI 		& BOLD 			& excitatory neurons 			& whole cortex & hp. stim. & iso. & mouse \\
		Pais-Roldan et al. 2020\cite{PaisRoldn2020} & FP 		& BOLD 			& neurons 				& cingulate cortex & pupil size correlated RS & $\alpha$-chloralose & rat \\ \bottomrule
	\end{tabular}
\end{sidewaystable}


\subsubsection*{Multimodal imaging for refining forward model}
\label{sec:fwm}
}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[width=\textwidth]{img/balloonmodel.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:balloon}
		\textbf{The canonical balloon model can be augmented to comprehensively account for signal modulation sources from the NVU.}
		Depicted is an extended balloon model integration non-neuronal contributions to the BOLD signal with the originally proposed model (gray shading).
		The stimulus pulse train prompts both neuronal and astrocytic activation (as illustrated by the Ca\textsuperscript{2+} transient) which lead to a respective change in CBF (f\textsubscript{n} , f\textsubscript{a} ) and changes in oxygen consumption (q\textsubscript{n} , q\textsubscript{a} ).
		These effects are lumped into cell-type specific hemodynamic response functions (HRF\textsubscript{n}, HRF\textsubscript{a}, see \cref{fig:astro}).
		In addition, stimulus-evoked changes in cardiac output (heart rate and/or blood pressure) may overrule cerebral autoregulation, prompting a non-specific CBF response (f\textsubscript{p}), which adds to the overall BOLD response.
	}
\end{figure}


The aim of noninvasive functional brain imaging is to provide an accurate estimate of the underlying neuronal activity.
Employing optical recordings at multiple stages of neurovascular coupling provides relevant information for optimizing forward modeling.
To generate a more comprehensive picture of the observed BOLD response, we suggest extending the well-known balloon model, which predicts the changing venous blood volume and dHb content (thereby prompting the BOLD response), by including non-neuronal mechanisms shown to display stimulus-associated signal transients (\cref{fig:balloon}).
The original model (see e.g. \cite{Stephan2007}) depends only on the inflow of blood, with neuronal signaling (i.e. neurotransmitters acting directly on the vasculature) typically assumed as the only driving force.
However, this fails to account for certain key features of the BOLD response, such as the prolonged response phase or post-stimulus undershoot.
Based on our previous research, we suggest two additional main sources associated with CBV changes: Astrocytic activity \cite{Schulz2012,Schlegel2018} leading to the release of vasoactive molecules, and depending on the physiological state potentially to altered cardiovascular activity causing a systemic change in blood flow that may overrule cerebral autoregulation.
The last point is of particular relevance for studies using anesthetized animals.
While the former two sources are part of intact neurovascular coupling, cardiovascular responses, such as stimulus-evoked sudden changes in heart rate and blood pressure \cite{Schroeter2014}, constitute a confounding factor that should be minimized \cite{Schlegel2015,Shim2018}.

Animals studies revealed interference of anesthesia on CNS physiology at various levels.
It was shown to affect neuronal excitability per se without affecting NVC \cite{Franceschini2010}, to compromise NVC \cite{Masamoto2012}, or to alter systemic cardiovascular output \cite{Schroeter2014}, or a combination of these effects.
In all these conditions, anesthesia would induce alterations in the stimulation induced FH, though the causes would be rather different, and correspondingly the relationship between neuronal signals and FH responses.
Such limitations have to be considered when using results obtained from anesthetized animals for the interpretation of data obtained in conscious humans.
Yet, a detailed discussion of these aspects is beyond the scope of this article.

A forward model of the neuronal-astrocytic-vascular Ca\textsuperscript{2+} signaling cascade has recently shown to accurately reproduce BOLD responses \cite{Tesler2021}.
However, the assumption of astrocytes as a passive intermediary between neurons and the vasculature fails to account for situations where the neuronal activity (but not necessarily the astrocytic activity) and the BOLD response are uncoupled or even inverted \cite{Takata2018,Sirotin2009}.
The hope of creating more accurate forward models is that, by inverting the model, accurate inferences about neuronal activity could be made solely based on the BOLD response.
Despite the growing evidence that the BOLD signal is not driven by neuronal activity alone, refined forward models could help define the boundaries within which the NVC is intact, and thus prevent erroneous interpretations of BOLD fMRI.
