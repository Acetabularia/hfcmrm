\section{Introduction}
\label{sec:i}

Noninvasive functional brain imaging has made significant contributions to the understanding of mammalian brain functional architecture and of its adaptation to drug interventions and changes imposed by pathological conditions.
Among such imaging modalities, blood oxygen level dependent (BOLD) functional magnetic resonance imaging (BOLD fMRI) is probably the most widely used surrogate measure of neural activity in humans \cite{Ogawa1990,Kwong1992,Ogawa1992}.
The method is attractive as it is noninvasive, does not require the administration of contrast agents, is fully 3-dimensional, and can provide whole brain coverage.
The contrast mechanism underlying the BOLD signal is based on activation-induced changes in the oxygen extraction from blood, which affects the ratio of diamagnetic oxygenated (Hb) to paramagnetic deoxygenated hemoglobin (dHb), with dHb acting as an intrinsic MRI contrast agent.
The resulting local change in magnetic susceptibility affects the transverse relaxation rates (R\textsubscript{2}, R\textsubscript{2}*), which leads to changes in signal intensity if R\textsubscript{2} (spin echo) or R\textsubscript{2}*-sensitive (gradient echo) fMRI acquisition protocols are being used.
Alternatively, activation-induced changes in cerebral blood flow (CBF) or blood volume (CBV) can be assessed using fMRI methods \cite{cbv}.
Despite these obvious advantages, it is important to realize that fMRI-based functional readouts measure hemodynamic alterations elicited by neural activity and therefore constitute an indirect measure of brain activity.
The validity of the surrogate depends on the integrity of the neurovascular (and neurometabolic) coupling (NVC), i.e. the link between neural activity and the local hemodynamic adaptation.
In fact, this is the case for many functional imaging methods assessing brain activity, such as intrinsic optical imaging \cite{Lieke1989}, near-infrared spectroscopic (NIRS) imaging \cite{Obrig2003}, label-free photoacoustic imaging \cite{Yao2013}, and ultrasound imaging \cite{Rabut2019}.
Sophisticated biophysical models have been developed to account for the dynamic behavior of the BOLD fMRI signal \cite{Buxton1997,Buxton1998,Stephan2007}, capable of describing BOLD responses in a semi-quantitative manner.
In these models, complex biological processes such as the triggering of the vasodilatory signal by neuronal activity are lumped into single parameters and thus do not provide insight into the cellular and molecular mechanisms responsible for functional hyperemia (FH).
However, detailed knowledge of the mechanisms underlying neurometabolic and neurovascular coupling is essential in order to understand the spatio-temporal behavior of functional signals and to correctly interpret fMRI responses, in particular under pathological conditions or when assessing responses to therapeutic interventions.
Apart from this basic question on the nature of the BOLD fMRI signal, deciphering the role of specific cell populations in information processing is essential for elucidating the underpinnings of brain networks, i.e. analyzing how specific cell-types affects brain wide functional responses.

{\color{ForestGreen}
Animal models play an important role in addressing these questions.
While most of the pioneering fMRI research has been done in monkeys and rats \cite{Logothetis2001,Glover1999}, improvements in MR sensitivity have made it possible to study the much smaller brain volume of mice.
With the mouse being the standard organism for most neuroscience applications, there is a plethora of both invasive and noninvasive analysis methods capable of providing highly specific information on the behavior of specific cell types and molecular actors involved.
}
For example, \textit{in vivo} microscopy techniques allow the deconvolution of macroscopically lumped FH effects into contributions of individual tissue components (e.g. microvascular segments) resolved in space and time \cite{Uhlirova2016}.
Further research \cite{Devor2011,Gagnon2016} suggested the development of bottom-up forward models linking sub-population-specific neuronal activity to experimentally accessible responses in various microvascular segments.
Such work would integrate cell-type activity into the FH response derived from non-invasive imaging (FH determining the change in BOLD contrast), thereby “bridging [the gap] from microscopic to macroscopic scales”.
While this conceptual framework is convincing and lays out a clear path for the elucidation of the neurovascular response, it faces substantial challenges.
Local microvascular responses may not be easily translated to FH as observed by noninvasive imaging, since the irrigation volume of an ‘active’ brain region may extend beyond the field-of-view of microscopy techniques, and effects occurring outside the FOV would also influence the FH.
Hence, information should be collected across scales and integrated across space and time to match the dimensions accessible by non-invasive imaging methods (e.g. \cite{Hillman2014}).
Moreover, extant \textit{in vivo} microscopy techniques may become challenging when studying deep brain structures.

Alternatively, information on the contribution of individual cell-types to FH may be obtained using cell-specific labelling strategies in combination with an imaging readout that collects data from a volume comparable to that of the noninvasive fMRI imaging method.
The hybrid setup can be designed such that multiple signals can be sampled simultaneously, which becomes critical under conditions in which scan-to-scan variability contains relevant biological information (see below).
Early hybrid experiments combining fMRI with well-established electrophysiological recordings provided valuable insights into the type of neuronal activity that is driving FH \cite{Logothetis2001}.
However, as electrophysiology lacks cell specificity and interferes with MRI, is has gradually been replaced by newly {\color{ForestGreen} developed} optical techniques.
Light can be guided in a straightforward manner into and out of the magnet, causing no or minimal interference with the fMRI experiment.
Further, optical reporter systems such as genetically-encoded calcium indicators (GECI) can be rendered cell-specific in a highly selective manner.
Thus, optical techniques can facilitate multimodal recordings of cell-specific contributions to neural processing.
Their major limitation arises from the interaction of light photons with biological tissue (scattering, attenuation), which limits tissue penetration.
Even when using the most favorable wavelength domain, light propagation becomes diffusive at distances exceeding a few hundred micrometers, and spatial information is rapidly lost.
Hence, optic recording procedures may need to be invasive, i.e. the brain region of interest may need to be exposed or accessed by inserting an optical fiber.
Fiber-based techniques are of particular interest when studying deep brain structure as they significantly reduce procedure invasiveness.

Apart from understanding the nature of the functional readout provided by fMRI and related readouts, identifying cell-specific contributions and understanding the interplay of different brain cells in controlling information processing across the brain is of key importance in neuroscience.
In addition to providing a non-interfering frequency band for complementary imaging, terahertz electromagnetic radiation such as visible light can be employed for stimulation.
This methodological variation enhances certainty in the study of brain function, as predefined signals can be introduced directly at the neuronal level in different brain structures.
In particular, this allows the targeted study of neuronal pathways of high biomedical relevance but with low endogenous signal, such as monoaminergic \cite{Lohani2016,Grandjean2019} and neuropeptidergic \cite{Caref2020} pathways.
Further, this method of study can complement the computational network modelling of the brain (e.g. \cite{Maia2015}) with \textit{in vivo} targeting of node-like systems (see, e.g. \cite{Grandjean2019}), within the spatio-temporal bounds of current methodological feasibility.

As the brain is not endogenously sensitive to light, primary signal transduction needs to be provided via an exogenous manipulation.
This is commonly done via expression of light-sensitive bacterial proteins, a process known as optogenetics, and in conjunction with fMRI, as opto-fMRI \cite{Lee2010}.

Not surprisingly, the introduction of optogenetics as a tool for highly specific interventions in the behaving animal already constituted a milestone for the study of brain functional architecture \cite{Boyden2005, Deisseroth2010}.
Moreover, after selective transfection of a specific cell population to express an optogenetic construct, it could be demonstrated that the functional network associated with the target population is discernible in fMRI readouts \cite{Lee2010,Desai2011}.

Hence, the combination of fMRI with optical methods is attractive both for linking the response of different CNS cell types to non-invasive imaging readouts, and for analyzing the effect of specific cell populations on information processing across the brain.
In this article, we will first discuss methodological aspects of hybrid fMRI and optical system integration, and then focus on these two aspects: multimodal recording of neuronal and astrocytic responses in combination with fMRI (at rest and during sensory stimulation), and imaging of effective connectivity patterns associated with specific neuronal populations.
For the latter part, we will particularly focus on serotonergic and dopaminergic signalling elicited by stimulation of the respective deep brain nuclei.
